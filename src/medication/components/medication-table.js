import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../../medication/api/medication-api";




class MedicationTable extends React.Component {

    constructor(props) {
        super(props);
        this.viewOlny = this.props.viewOnly;
        this.reloadHandler1 = this.props.reloadHandler1;
        this.state = {
            tableData: this.props.tableData
        };
    }

    columns1 = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Side Effects',
            accessor: 'sideEffects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
        {
            Header: ' ',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor: 'red', borderStyle: 'none', color: 'white'}} onClick={()=>{ console.log(cell.value);
                this.handleDelete(cell.value);
            }}> Delete </button>)
        },

        {
            Header: ' ',
            accessor: 'id',

            Cell:(cell)=>(<button style={{backgroundColor: 'green', borderStyle: 'none', color: 'white'}} onClick={()=>{

                this.handleId(cell.value);
            }}> Update </button>)

        },


    ];

    columns2 = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Side Effects',
            accessor: 'sideEffects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },



    ];



    filters = [
        {
            accessor: 'name',
        }
    ];

    handleId(id) {

        this.props.id(id)
        console.log(id);
        return id;

    }

    handleDelete(id) {
        return API_USERS.deleteMedication(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted medication with id: " + result);
                this.reloadHandler1();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.viewOlny === 1 ? this.columns2:this.columns1}
                search={this.filters}
                pageSize={5}
                onChange={this.handleId}
            />
        )
    }
}

export default MedicationTable;