import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import PatientContainer from './patient/patient-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import CaregiverContainer from "./caregiver/caregiver-container";
import MedicationContainer from "./medication/medication-container";


import Login from "./pages/login";
import Caregiver from "./pages/caregiver";
import Patient from "./pages/patient";
import BackgroundImg from "./commons/images/medical1.jpg";
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1200px",
    backgroundImage: `url(${BackgroundImg})`,

};
class App extends React.Component {


    render() {

        return (

            <div style={backgroundStyle}>
            <Router>
                <div>

                    <Switch>

                        <Route
                            exact
                            path='/doctor'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            component={Caregiver}
                        />

                        <Route
                            exact
                            path='/patient'
                            component={Patient}
                        />

                        <Route
                            exact
                            path='/doctor/patient'
                            render={() => <PatientContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor/caregiver'
                            render={() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor/medication'
                            render={() => <MedicationContainer/>}
                        />

                        <Route
                            exact
                            path='/'
                            render={() => <Login/>}/>{""}
                        />
                        <Route
                            exact
                            path='/login'
                            render={() => <Login/>}/>{""}
                        />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>

        )
    };
}

export default App
