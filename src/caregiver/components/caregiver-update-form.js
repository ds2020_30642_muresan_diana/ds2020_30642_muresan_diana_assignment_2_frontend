import React from 'react';
import validate from "./validators/caregiver-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import {getCaregiverById} from "../api/caregiver-api";

class CaregiverUpdateForm extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.caregiver = this.props.caregiver;
        console.log(this.caregiver);
        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: true,

            formControls: {
                id: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                name: {
                    value: '',
                    placeholder:  '' ,
                    valid: false,
                    touched: false

                },
                birthdate: {
                    value:  '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                gender: {
                    value:  '',
                    placeholder:  '',
                    valid: false,
                    touched: false

                },
                address: {
                    value:   '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },
                username: {
                    value:   '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },
                password: {
                    value:   '',
                    placeholder:  '',
                    valid: false,
                    touched: false,
                },



            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;

        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid

        }

        this.setState({
            formControls: updatedControls,
            formIsValid: true
        });
    };

    updateCaregiver(caregiver) {
        return API_USERS.updateCaregiver(caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated caregiver with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleEdit() {
        console.log(this.caregiver.name);
        let name, birthdate, gender, address, username, password = null;
        if (this.state.formControls.name.value === '' )
            name = this.caregiver.name;
        else name = this.state.formControls.name.value;
        if (this.state.formControls.birthdate.value === '' )
            birthdate = this.caregiver.birthdate;
        else birthdate = this.state.formControls.birthdate.value;
        if (this.state.formControls.gender.value === '' )
            gender = this.caregiver.gender;
        else gender = this.state.formControls.gender.value;
        if (this.state.formControls.address.value === '' )
            address = this.caregiver.address;
        else address = this.state.formControls.address.value;
        if (this.state.formControls.username.value === '' )
            username = this.caregiver.username;
        else username = this.state.formControls.username.value;
        if (this.state.formControls.password.value === '' )
            password = this.caregiver.password;
        else password = this.state.formControls.password.value;

        let caregiver = {
            id: this.caregiver.id,
            name: name,
            birthdate: birthdate,
            gender: gender,
            address: address,
            username: username,
            password: password
        };


        console.log(caregiver);
        this.updateCaregiver(caregiver);
    }

    render() {

        return (
            <div>
                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.caregiver.name}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}

                        // valid={this.state.formControls.name.valid}

                    />

                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate: </Label>
                    <Input name='birthdate' id='birthdateField' placeholder={this.caregiver.birthdate}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birthdate.value}
                           touched={this.state.formControls.birthdate.touched? 1 : 0}
                        //valid={this.state.formControls.birthdate.valid}

                    />
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.caregiver.gender}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                        //valid={this.state.formControls.gender.valid}

                    />

                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.caregiver.address}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                        //valid={this.state.formControls.address.valid}

                    />
                </FormGroup>

                <FormGroup id='username'>
                    <Label for='usernameField'> Username: </Label>
                    <Input name='username' id='usernameField' placeholder={this.caregiver.username}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched? 1 : 0}
                        //valid={this.state.formControls.address.valid}

                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.caregiver.password}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                        //valid={this.state.formControls.address.valid}

                    />
                </FormGroup>



                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleEdit}> Edit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>

                }


            </div>
        );

    }


}

export default CaregiverUpdateForm;