import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../../caregiver/api/caregiver-api";




class CaregiverTable extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler1 = this.props.reloadHandler1;
        this.state = {
            tableData: this.props.tableData
        };
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Password',
            accessor: 'password',
        },
        {
            Header: ' ',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor: 'red', borderStyle: 'none', color: 'white'}} onClick={()=>{ console.log(cell.value);
                this.handleDelete(cell.value);
            }}> Delete </button>)
        },

        {
            Header: ' ',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor: 'green', borderStyle: 'none', color: 'white'}} onClick={()=>{
            this.handleId(cell.value, 1);
            }}> Update </button>)
        },
        {
            Header: ' ',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor:'blue', borderStyle: 'none', color: 'white'}} onClick={() => {
            this.handleId(cell.value, 0);
            }}> Patients </button>)
        }



    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    handleDelete(id) {
        return API_USERS.deleteCaregiver(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted patient with id: " + result);
                this.reloadHandler1();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleId(id, val) {
        let params = [id, val];
        this.props.params(params);
        return params;

    }

    render() {
        return (

            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
                onChange={this.handleId}
            />
        )
    }
}

export default CaregiverTable;