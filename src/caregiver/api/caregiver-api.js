import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    caregiver: '/caregiver'
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverByName(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/get-by-name/' + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverByUsername(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/get-by-username/' + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}
function deleteCaregiver(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + "/" + params, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}
function updateCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}



export {
    getCaregivers,
    getCaregiverById,
    postCaregiver,
    deleteCaregiver,
    updateCaregiver,
    getCaregiverByName,
    getCaregiverByUsername
};