import React from 'react';

import BackgroundImg from '../commons/images/medical1.jpg';

import {Button, Card, Col, Container, DropdownItem, Jumbotron, NavLink, Row} from 'reactstrap';
import NavigationBar from "../navigation-bar";
import {Redirect} from "react-router-dom";


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };



class Home extends React.Component {

    constructor(props){
        super(props);
        //console.log(props);
        //console.log(props.location.state.username);
        this.state = {
            logout: false
        };
        this.handleLogout = this.handleLogout.bind(this);
    }


    handleLogout() {
        this.setState( {
            logout: true
        })
    }


    render() {
        const { logout } = this.state;
        if ( logout === true ) {
            localStorage.removeItem('user');
            return (<Redirect to={{
                pathname: '/login'

            }} />)
        }
        if (localStorage.getItem('user') === "doctor")
            return (

            <div style={backgroundStyle}>

                <div className={"nav-bar"}>
                    <div className={"nav-bar-item-container"}>
                        <div className={"nav-bar-box"}>
                            <a className={"nav-bar-item"} href="/doctor/patient">Patients</a>
                        </div>
                    </div>
                    <div className={"nav-bar-item-container"}>
                        <div className={"nav-bar-box"}>
                            <a className={"nav-bar-item"} href="/doctor/caregiver">Caregivers</a>

                        </div>
                    </div>
                    <div className={"nav-bar-item-container"}>
                        <div className={"nav-bar-box"}>
                            <a className={"nav-bar-item"} href="/doctor/medication">Medications</a>

                        </div>
                    </div>
                </div>

                <button className={"logout-button"} onClick={this.handleLogout}> Logout</button>


            </div>
        )
        else  return (<Redirect to={{
            pathname: '/login'

        }} />)
    };
}

export default Home
