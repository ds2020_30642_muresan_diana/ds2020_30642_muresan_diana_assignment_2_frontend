import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col, FormGroup, Input, Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';



import PatientTable from "../patient/components/patient-table";
import * as API_USERS from "../patient/api/patient-api"
import {Redirect} from "react-router-dom";
import Table from "../commons/tables/table";
import PatientForm from "../patient/components/patient-form";
import PatientUpdateForm from "../patient/components/patient-update-form";
import MedicationPlanForm from "../patient/components/medication-plan-form";
import MedicationTable from "../medication/components/medication-table";


class Patient extends React.Component {


    columns = [

        {
            Header: 'Intake Intervals',
            accessor: 'intakeIntervals',

        },
        {
            Header: 'Period of Treatment',
            accessor: 'periodOfTreatment',

        },


        {
            Header: ' ',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor: 'blue', borderStyle: "none", color: 'white'}} onClick={()=>{ console.log(cell.value);
                this.handleViewMeds(cell.value);
            }}> View Medications </button>)
        },
    ];
    filters = [
        {
            accessor: 'periodOfTreatment',
        }
    ];
    constructor(props) {
        super(props);
        this.sentData = props.location;
        //this.username = props.location.state.username;
       // console.log(props);
        //console.log(props.location.state.username);
        this.reload = this.reload.bind(this);
        this.toggleMeds = this.toggleMeds.bind(this);
        this.handleLogout =  this.handleLogout.bind(this);
        this.medicationPlansList = [];



        this.medicationPlan = {
            id: null,
            intakeIntervals: null,
            periodOfTreatment: null,
            medications : null
        }
        this.medication = {
            id: null,
            name: null,
            sideEffects: null,
            dosage: null
        };


        this.patient = {
            id: null,
            name: null,
            birthdate: null,
            gender: null,
            address: null,
            medicalRecord: null,
            username: null,
            password: null,
            medicationPlans: null

        }
        this.state = {

            selected: false,
            logout: false,
            tableData: [],
            medications: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        };
    }

    componentDidMount() {
        if (localStorage.getItem('user') === "patient")
            this.fetchPatient(this.sentData.state.username);
    }

    reload() {
        this.setState({
            isLoaded: false
        });

        this.fetchPatient();
    }

    toggleMeds() {
        this.setState({selected: !this.state.selected});

    }

    handleViewMeds(idMedPlan) {
        for (let i = 0; i<this.patient.medicationPlans.length; i++) {
            if (this.patient.medicationPlans[i].id === idMedPlan) {
                this.state.medications = this.patient.medicationPlans[i].medications;
                this.state.showDetails = true;
                this.toggleMeds();
                break;
            }

        }
    }

    fetchPatient(username) {
        return API_USERS.getPatientByUsername(username,(result, status, err) => {

            if (result !== null && status === 200) {
                this.patient.id = result.id;
                this.patient.name = result.name;
                this.patient.address = result.address;
                this.patient.gender = result.gender;
                this.patient.birthdate = result.birthdate;
                this.patient.medicalRecord = result.medicalRecord;
                this.patient.username = result.username;
                this.patient.password = result.password;
                this.patient.medicationPlans = result.medicationPlans;

                for(let j= 0; j < this.patient.medicationPlans.length; j++) {
                    let medicationPlan = {
                        id: null,
                        intakeIntervals: null,
                        periodOfTreatment: null,
                        medications : null
                    }
                        medicationPlan.id = result.medicationPlans[j].id;
                        medicationPlan.intakeIntervals = result.medicationPlans[j].intakeIntervals;
                        medicationPlan.periodOfTreatment = result.medicationPlans[j].periodOfTreatment;

                    console.log('nr medicamente' + result.medicationPlans[j].medications.length);
                    let medicationString = "";
                    for (let i = 0; i < result.medicationPlans[j].medications.length; i++) {
                        this.medication.id = result.medicationPlans[j].medications[i].id;
                        this.medication.name = result.medicationPlans[j].medications[i].name;
                        medicationString += "Name: " + this.medication.name;
                        this.medication.sideEffects = result.medicationPlans[j].medications[i].sideEffects;
                        medicationString += " Side Effects: " + this.medication.sideEffects;
                        this.medication.dosage = result.medicationPlans[j].medications[i].dosage;
                        medicationString += " Dosage: " + this.medication.dosage ;

                        console.log("string med" + i + medicationString);

                    }
                     medicationPlan.medications = medicationString;

                    this.medicationPlansList.push( medicationPlan);

                }
                console.log('plan' + this.medicationPlan);

                this.setState({
                    tableData: [result],
                    isLoaded: true,

                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleLogout() {
        this.setState( {
            logout: true
        })
    }

    render() {
        const { logout } = this.state;
        if ( logout === true ) {
            localStorage.removeItem('user');
            return (<Redirect to={{
                pathname: '/login'

            }} />)
        }
        if (localStorage.getItem('user') === "patient")

            return (
            <div>
                <CardHeader>
                    <span class={"info-title"}><strong> Your Info </strong></span>
                    <button className={"logout-button"} onClick={this.handleLogout}> Logout</button>
                </CardHeader>
                <br></br>
                <br></br>
                <div className={"info-container"}>
                    <div className={"patient-info"}>
                        <Label>Name: </Label>
                        <span>{this.patient.name}</span>
                    </div>
                    <div className={"patient-info"}>
                        <Label>Address: </Label>
                        <span>{this.patient.address}</span>
                    </div>
                    <div className={"patient-info"}>
                        <Label>Gender: </Label>
                        <span>{this.patient.gender}</span>
                    </div>
                    <div className={"patient-info"}>
                        <Label>Birthdate: </Label>
                        <span>{this.patient.birthdate}</span>
                    </div>
                    <div className={"patient-info"}>
                        <Label>Medical Record: </Label>
                        <span>{this.patient.medicalRecord}</span>
                    </div>
                    <div className={"patient-info"}>
                        <Label>Username: </Label>
                        <span>{this.patient.username}</span>
                    </div>

                </div>

                <br></br>
                <br></br>

                <CardHeader>
                    <span class={"med-plan-title"}><strong> Your Medication Plans </strong></span>

                </CardHeader>

                <br></br>
                <br></br>

                <div className={"table-background"}>
                <Table
                    data={this.medicationPlansList}
                    columns={ this.columns}
                    search={this.filters}
                    pageSize={5}
                />
                </div>
                <div>
                   </div>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleMeds}> Your Medications: </ModalHeader>

                    <ModalBody>
                             <MedicationTable tableData={this.state.medications} viewOnly={1}/>
                    </ModalBody>
                </Modal>


                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
        )

        else  return (<Redirect to={{
            pathname: '/login'

        }} />)




    }
}


export default Patient;
