import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    patient: '/patient'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "/" + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(user,caregiverName,  callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "/insert/" + caregiverName, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function deletePatient(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "/" + params, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function updatePatient(user,caregiverName, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "/" + caregiverName, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}


function getPatientByUsername(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/get-by-username/' + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientsByCaregiver(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/find-by-caregiver/' + username, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientsByCaregiverId(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/find-by-caregiver-id/' + id, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    getPatientById,
    postPatient,
    deletePatient,
    updatePatient,

    getPatientByUsername,
    getPatientsByCaregiver,
    getPatientsByCaregiverId
};